-- MySQL Script
-- Author : Diogo Vieira Ferreira
-- Project : Chat TPI
-- Date : 08.05.19

-- ------------------------------------------------------
-- Création de l'utilisateur pour l'application Chat
-- ------------------------------------------------------
CREATE USER if NOT EXISTS 'ChatAdmin'@'%' IDENTIFIED BY 'Pa$$w0rd';
GRANT ALTER ROUTINE, ALTER, SELECT, EXECUTE, CREATE TABLESPACE, CREATE ROUTINE, CREATE, CREATE VIEW, CREATE TEMPORARY TABLES, INDEX, EVENT, DROP, TRIGGER, REFERENCES, INSERT, SHOW DATABASES, PROCESS, SHOW VIEW, DELETE  ON *.* TO 'ChatAdmin'@'%';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'ChatAdmin'@'%';

DROP DATABASE IF EXISTS `Chat`;
CREATE DATABASE `Chat`;

USE `Chat`;
-- -----------------------------------------------------
-- Table User
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) UNIQUE NOT NULL,
  `ip` VARCHAR(45) NOT NULL DEFAULT "",
  `status` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`idUser`)
);

-- -----------------------------------------------------
-- Table Message
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Message` ;

CREATE TABLE `Message` (
  `idMessage` INT NOT NULL AUTO_INCREMENT,
  `fkUserFrom` INT NOT NULL,
  `fkUserTo` INT NOT NULL,
  `message` TEXT NOT NULL,
  `time` DATETIME NOT NULL,
  `read` TINYINT NOT NULL,
  PRIMARY KEY (`idMessage`),
  CONSTRAINT `fkUserFrom`
    FOREIGN KEY (`fkUserFrom`)
    REFERENCES `User` (`idUser`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fkUserTo`
    FOREIGN KEY (`fkUserTo`)
    REFERENCES `User` (`idUser`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table Log
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Log` ;

CREATE TABLE `Log` (
  `idLog` INT NOT NULL AUTO_INCREMENT, 
  `username` VARCHAR(45) NOT NULL,
  `information` TEXT NOT NULL,
  `time` DATETIME NOT NULL,
  PRIMARY KEY (`idLog`)
);

