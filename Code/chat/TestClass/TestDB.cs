using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System;
using System.Collections.Generic;

namespace chat
{
    ///<summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          TestDB
    ///   Description:    V�rifie la connection de la base de donn�es
    ///                   et l'insertion d'utilisateurs.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    [TestClass]
    public class TestDB
    {
        private DB _db;
        /// <summary>
        /// initialisation de la base de donn�es
        /// </summary>
        [TestInitialize]
        public void initialize()
        {
            _db = new DB("172.17.101.71", "Chat", "ChatAdmin", "Pa$$w0rd");
        }
        /// <summary>
        /// on v�rifie que nous avons bien pu modifier la table user
        /// </summary>
        [TestMethod]
        public void TestNetworkStatusSuccess()
        {
            User user = null;
            _db.NetworkStatus("x.x.x.x", true);           

            foreach (User users in _db.LstOfUsers())
            {
                if (users.Status == "1")
                    user = users;
            }

            Assert.AreEqual(Environment.UserName, user.Name);
        }
        /// <summary>
        /// on regarde que notre utilisateur a les m�me donn�es que celles ins�r�es dans la DB
        /// </summary>
        [TestMethod]
        public void TestLstUserSuccess()
        {
            List<User> lstUsersActual = new List<User>();
            User user = new User { Name = Environment.UserName, IP = "x.x.x.x", Status = "1", MessagesMissed = 0, totMsg = 0 };
            lstUsersActual.Add(user);
            _db.NetworkStatus("x.x.x.x", true);
            List<User> lstUsersExcepted = _db.LstOfUsers();
            CollectionAssert.Contains(lstUsersExcepted, user);
        }
        /// <summary>
        /// on met notre statut en d�connect� sur la DB
        /// </summary>
        [TestCleanup]
        public void Cleanup()
        {
            _db.NetworkStatus("", false);
        }
    }
}
