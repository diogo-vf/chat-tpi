using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System;
using System.Collections.Generic;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          TestUser
    ///   Description:    Permet de v�rifier que la Classe User aie les bonnes donn�es
    ///                   apr�s l'insertion des donn�es.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    [TestClass]
    public class TestUser
    {
        private User _user;
        private string _username;
        private string _ip;
        private string _status;
        private int _messagesMissed;

        /// <summary>
        /// Initialisation de toutes les variable pour les tests
        /// </summary>
        [TestInitialize]
        public void initialize()
        {
            _username = "UserTest";
            _ip = "8.8.8.8";
            _status = "1";
            _messagesMissed = 2;
            _user = new User();
        }
        /// <summary>
        /// v�rification que le nom de la personne ajout�e est correct
        /// </summary>
        [TestMethod]
        public void TestNameOfUserSuccess()
        {
            _user.Name = _username;

            Assert.AreEqual(_username, _user.Name);
        }
        /// <summary>
        /// v�rification que l'IP ajout�e est la bonne
        /// </summary>
        [TestMethod]
        public void TestIPOfUserSuccess()
        {
            _user.IP = _ip;

            Assert.AreEqual(_ip, _user.IP);
        }
        /// <summary>
        /// v�rification que le staut soit le bon
        /// </summary>
        [TestMethod]
        public void TestStatusOfUserSuccess()
        {
            _user.Status = _status;

            Assert.AreEqual(_status, _user.Status);
        }
        /// <summary>
        /// v�rification du bon nombre de messsages manqu�s
        /// </summary>
        [TestMethod]
        public void TestMsgMissedOfUserSuccess()
        {
            _user.MessagesMissed = _messagesMissed ;

            Assert.AreEqual(_messagesMissed, _user.MessagesMissed);
        }
    }
}
