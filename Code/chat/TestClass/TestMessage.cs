using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System;
using System.Collections.Generic;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          TestMessage
    ///   Description:    On ins�re des donn�es dans la classe Message, puis
    ///                   on v�rifie qu'elles sont rest�es intacts.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    [TestClass]
    public class TestMessage
    {
        private Message _message;
        private string _msgExcepted;
        private string _to;
        private string _from;
        /// <summary>
        /// itialisation des variables pour les tests
        /// </summary>
        [TestInitialize]
        public void initialize()
        {
            _message = new Message();
            _msgExcepted = "message de test";
            _to = "Diogo";
            _from = "Application";
        }
        /// <summary>
        /// v�rification que ce soit le bon exp�diteur 
        /// </summary>
        [TestMethod]
        public void TestFromSuccess()
        {
            _message.From = _from;

            Assert.AreEqual(_from, _message.From);
        }
        /// <summary>
        /// v�rification de l'envoie � la bonne personne
        /// </summary>
        [TestMethod]
        public void TestToSuccess()
        {
            _message.To = _to;

            Assert.AreEqual(_to, _message.To);
        }
        /// <summary>
        /// v�rification du bon message
        /// </summary>
        [TestMethod]
        public void TestMessageSuccess()
        {
            _message.Msg=_msgExcepted;

            Assert.AreEqual(_msgExcepted,_message.Msg);
        }

    }
}
