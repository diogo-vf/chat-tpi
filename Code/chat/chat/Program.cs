﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace chat
{
    static class Program
    {
        /// <summary>
        ///-------------------------------------------------------------------------------------------------------------------
        ///
        ///   Namespace:      Chat
        ///   Class:          Program
        ///   Description:    Cette classe permet de démarrer notre application.
        ///   Author:         Diogo Vieira Ferreira
        ///   Date:           28.05.19
        ///   
        ///-------------------------------------------------------------------------------------------------------------------
        /// </summary>
        [STAThread]
        static void Main()
        {
            Controler ctrl = new Controler();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(ctrl.CreateForm());
        }
    }
}
