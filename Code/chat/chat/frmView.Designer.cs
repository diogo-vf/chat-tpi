﻿using System.Windows.Forms;
using System.Collections.Generic;
namespace chat
{
    partial class frmView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        /// <summary>
        /// permet de voir de manière graphique où 
        /// et comment sera placé chaque composant graphique
        /// avant le lancement de l'application sur l'IDE
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.notification = new System.Windows.Forms.NotifyIcon(this.components);
            // 
            // notification
            // 
            this.notification.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notification.Icon = global::chat.Properties.Resources.icoChat;
            this.notification.Text = "Nouveau Message!";
            this.notification.Visible = true;

            this.Icon = global::chat.Properties.Resources.icoChat;
        }
        /// <summary>
        /// affichage de la première interface
        /// (Liste des personnes utilisant l'application)
        /// </summary>
        public void FirstInterface()
        {
            this.Icon = chat.Properties.Resources.icoChat;
            // 
            // lblConnected
            // 
            this.lblConnected = new System.Windows.Forms.Label();
            this.lblConnected.AutoSize = true;
            this.lblConnected.Location = new System.Drawing.Point(20, 0);
            this.lblConnected.Name = "lblConnected";
            this.lblConnected.Size = new System.Drawing.Size(0, 13);
            this.lblConnected.TabIndex = 0;
            this.lblConnected.Text = System.Environment.UserName + " Vous êtes connecté!";

            // 
            // lblInfo
            // 
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(34, 34);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(351, 13);
            this.lblInfo.TabIndex = 2;
            this.lblInfo.Text = "Double-cliquez sur le nom  de la personne avec qui vous voulez discuter:";
            // 
            // lstUsers
            // 
            this.lstUsers = new System.Windows.Forms.ListBox();
            this.lstUsers.FormattingEnabled = true;
            this.lstUsers.Location = new System.Drawing.Point(37, 50);
            this.lstUsers.Name = "lstUsers";
            this.lstUsers.Size = new System.Drawing.Size(428, 316);
            this.lstUsers.TabIndex = 1;
            this.lstUsers.DoubleClick += _ctrl.lstUsers_DoubleClick;

            // 
            // Timer
            // 
            refreshTimer = new Timer();
            refreshTimer.Tick += _ctrl.TimerFillLstUsers;
            refreshTimer.Interval = 5000;
            refreshTimer.Start();

            // 
            // frmView
            // 
            this.ClientSize = new System.Drawing.Size(500, 429);
            this.Controls.Add(this.lstUsers);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblConnected);
            this.FormClosed += (sender, EventArgs) => { _ctrl.frmView_FormClosed(sender, EventArgs, this); }; ;
        }
        /// <summary>
        /// deuxième interface
        /// (Messagerie entre destinataire et nous)
        /// </summary>
        public void SecondInterface()
        {
            this.Icon = chat.Properties.Resources.icoChat;
            this.FormClosing += (sender, EventArgs) => { _ctrl.frmChat_clossing(sender, EventArgs, this); };
            this.rtbReader = new System.Windows.Forms.RichTextBox();
            this.rtbSender = new System.Windows.Forms.RichTextBox();
            this.lblSender = new System.Windows.Forms.Label();
            this.cmdSender = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbReader
            // 
            this.rtbReader.BackColor = System.Drawing.Color.White;
            this.rtbReader.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbReader.Location = new System.Drawing.Point(12, 12);
            this.rtbReader.Name = "rtbReader";
            this.rtbReader.ReadOnly = true;
            this.rtbReader.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbReader.Size = new System.Drawing.Size(563, 241);
            this.rtbReader.TabIndex = 0;
            this.rtbReader.Text = "";
            // 
            // rtbSender
            // 
            this.rtbSender.Location = new System.Drawing.Point(12, 294);
            this.rtbSender.Name = "rtbSender";
            this.rtbSender.Size = new System.Drawing.Size(563, 89);
            this.rtbSender.TabIndex = 1;
            this.rtbSender.Text = "";
            // 
            // lblSender
            // 
            this.lblSender.AutoSize = true;
            this.lblSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSender.Location = new System.Drawing.Point(12, 266);
            this.lblSender.Name = "lblSender";
            this.lblSender.Size = new System.Drawing.Size(156, 24);
            this.lblSender.TabIndex = 2;
            this.lblSender.Text = "Votre message:";
            // 
            // cmdSender
            // 
            this.cmdSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSender.Location = new System.Drawing.Point(447, 387);
            this.cmdSender.Name = "cmdSender";
            this.cmdSender.Size = new System.Drawing.Size(128, 35);
            this.cmdSender.TabIndex = 3;
            this.cmdSender.Text = "Envoyer";
            this.cmdSender.UseVisualStyleBackColor = true;
            this.cmdSender.Click += (sender, EventArgs) => { _ctrl.frmChat_cmdSender_Click(sender, EventArgs, this); };
            // 
            // Timer
            // 
            timerStatus = true;            
            tmrMsg = new Timer();
            tmrMsg.Tick += (sender, EventArgs) => { _ctrl.frmChat_tmrMsg_Timer(sender, EventArgs, this); };
            tmrMsg.Interval = 100;
            tmrMsg.Start();
            // 
            // frmView
            // 
            this.ShowInTaskbar = true;
            this.ClientSize = new System.Drawing.Size(587, 426);
            this.Controls.Add(this.cmdSender);
            this.Controls.Add(this.lblSender);
            this.Controls.Add(this.rtbSender);
            this.Controls.Add(this.rtbReader);
            this.ResumeLayout(false);
            this.PerformLayout();

            this.ActiveControl = rtbSender;
        }

        private System.Windows.Forms.Label lblConnected;
        /// <summary>
        /// listbox contenant tous les utilisateurs connectés
        /// </summary>
        public ListBox lstUsers;
        /// <summary>
        /// RichTextBox contenant tous les messages
        /// </summary>
        public RichTextBox rtbReader;
        /// <summary>
        /// richTextBox permettant d'envoyer des messages
        /// </summary>
        public RichTextBox rtbSender;
        private Label lblSender;
        /// <summary>
        /// permet de désactiver le bouton du form lorque le destinataire a quitté l'application
        /// </summary>
        public Button cmdSender;
        /// <summary>
        /// permet de stoper le timer lors de la fermeture de l'application par le biai du controler
        /// </summary>
        public Timer refreshTimer;
        /// <summary>
        /// permet de stoper le timer lors de la fermeture de l'application par le biai du controler
        /// timer pour afficher les messages
        /// </summary>
        public Timer tmrMsg;
        /// <summary>
        /// permet d'empecher les timers de raffraichir les données au moment de la fermeture
        /// de l'application
        /// </summary>
        public bool timerStatus;
        /// <summary>
        /// permet de gérer les notifications
        /// </summary>
        public NotifyIcon notification;
        private Label lblInfo;
    }
}

