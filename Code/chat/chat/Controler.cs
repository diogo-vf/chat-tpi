﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          Controler
    ///   Description:    Permet de gérer tous les évènements entre la vue et le model.
    ///                   Toutes les données doivent passer par l'intermédiaire du controler
    ///                   avant d'être utilisées par le model ou affichées par la vue (frmView).
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    public class Controler
    {
        private Model _model;
        private frmView _frmView;
        private Thread _thReader;
        private List<User> _lstOfUsers;
        private List<frmView> _lstChats;
        private bool recipientExit = false;
        #region Constructor
        /// <summary>
        /// Cette méthode est la méthode principale du controler
        /// Elle permet de créer la vue et d'initier les différentes classe:
        /// Model et frmView (la vue) 
        /// </summary>
        /// <returns></returns>
        public frmView CreateForm()
        {
            try
            {
                _lstOfUsers = new List<User>();
                _lstChats = new List<frmView>();
                this._frmView = new frmView(this, "Messagerie", "frmAccueil");
                this._frmView.FirstInterface();
                this._model = new Model();
                this._model.StartNetwork();
                this._thReader = new Thread(this.NetworkReader);
                this._thReader.Start();                

                FillLstUsers();
            }
            catch (Exception ex)
            {
                MessageBox.Show( "Veuillez reporter le message suivant:\n\n Form: " + ex.Message, "Erreur!");
            }
            return this._frmView;
        }
        #endregion Constructor
        #region Private methods
        /// <summary>
        /// Méthode permettant d'écouter en continu le réseau et de traiter les informations
        /// </summary>
        private void NetworkReader()
        {
            while (_model.NtwRunning)
            {
                try
                {
                    _model.NetworkReader();
                    _model.NetworkReceiver();
                    if (!string.IsNullOrEmpty(_model.MessageFrom))
                    {
                        Notification(_model.MessageFrom);                        
                    }
                }
                catch (Exception)
                {
                }
            }
        }
        /// <summary>
        /// permet de générer une notification lors de la reception d'un message 
        /// </summary>
        /// <param name="username"></param>
        private void Notification(string username)
        {
            frmView frm;
            try
            {
                frm = Application.OpenForms.Cast<frmView>().Where(x => x.Text == username).ElementAt(0);
            }
            catch (Exception)
            {
                frm = null;
            }
            bool ok;
            
            if (frm == null || frm.WindowState == FormWindowState.Minimized || frm.IsAccessible )
            {
                ok = true;
            }
            else
            {
                ok = false;
            }

            if(ok)
            {
                _frmView.notification.BalloonTipTitle = "Nouveau message de " + username;
                _frmView.notification.BalloonTipText = "Vous avez reçu un nouveau message.";
                _frmView.notification.ShowBalloonTip(1000);
                _model.SaveDiscution(_model.LstOfMessage, username, false);
            }

        }
       
        /// <summary>
        /// vide les données dans la liste des utilisateurs
        /// récupère une liste d'utilisateurs et affiche seulement les utilisateurs connectés
        /// </summary>
        private void FillLstUsers()
        {
            try
            {
                if(!_lstOfUsers.Equals(_model.lstOfUsers))
                {
                    this._frmView.lstUsers.Items.Clear();
                    foreach (User user in _model.lstOfUsers)
                    {
                        if(_lstOfUsers.Contains(user))
                        {
                            _lstOfUsers[_lstOfUsers.FindIndex(ind => ind.Equals(user))].MessagesMissed = user.MessagesMissed;
                            _lstOfUsers[_lstOfUsers.FindIndex(ind => ind.Equals(user))].Status = user.Status;
                            _lstOfUsers[_lstOfUsers.FindIndex(ind => ind.Equals(user))].IP = user.IP;
                        }
                        else
                            _lstOfUsers.Add(user);

                        if (user.Status == "1" && user.Name != Environment.UserName)
                            this._frmView.lstUsers.Items.Add("Connecté - " + user.Name + " (" + user.MessagesMissed + " messages manqués)");
                        else if (user.Status == "0" && user.Name != Environment.UserName)
                            this._frmView.lstUsers.Items.Add("Déconnecté - " + user.Name + " (" + user.MessagesMissed + " messages manqués)");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( "Veuillez reporter le message suivant:\n\n FillUser: " + ex.Message, "Erreur!");
            }
        }
        /// <summary>
        /// Pour chaque personne visible sur la page d'accueil,
        /// on crée lui prépare une messagerie en avance.
        /// </summary>
        /// <param name="person"></param>
        private void UpdateLstChat(string person)
        {
            try
            {
                bool ok = true;
                foreach (frmView frmView in _lstChats)
                {
                    if (frmView.Text == person)
                        ok = false;
                }
                if (ok)
                    _lstChats.Add(new frmView(this, person, _lstOfUsers.Find(x => x.Name == person).IP));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Veuillez reporter le message suivant:\n\n Lst Chat:" + ex.Message, "Erreur!");
            }
        }
        /// <summary>
        /// on ouvre ou on remet le focus sur un des chat préparés
        /// par la métohde UpdateLstChat 
        /// </summary>
        /// <param name="destinataire"></param>
        private void OpenChat(string destinataire)
        {
            try
            {
                if (_lstChats.Find(x => x.Text == destinataire) != null)
                {
                    frmView frmView;
                    if (Application.OpenForms.Cast<frmView>().Any(x => x.Text == destinataire))
                    {
                        frmView = Application.OpenForms.Cast<frmView>().Where(x => x.Text == destinataire).ElementAt(0);
                        frmView.Focus();
                        frmView.Location = new System.Drawing.Point((Screen.PrimaryScreen.WorkingArea.Width - frmView.Width) / 2,
                                                                  (Screen.PrimaryScreen.WorkingArea.Height - frmView.Height) / 2);
                    }
                    else
                    {
                        frmView = _lstChats.Find(x => x.Text == destinataire);
                        frmView.SecondInterface();
                        frmView.Show();
                    }
                    foreach (Message message in _model.GetHistory(destinataire))
                    {
                        _model.LstOfMessageHistory.Add(message);
                        frmView.rtbReader.AppendText("\r\n" + message.From + ": " + message.Msg);
                        frmView.rtbReader.ScrollToCaret();
                    }
                    _model.DiscutWith(destinataire);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Veuillez reporter le message suivant:\n\n Ouverture:" + ex.Message, "Erreur!");
            }
        }
        #endregion Private methods
        #region Public methods
        /// <summary>
        /// met à jour la liste des utilisateur
        /// connectés toutes les 5 sec
        /// </summary>
        public void TimerFillLstUsers(object sender, EventArgs e)
        {
            FillLstUsers();
        }
        /// <summary>
        /// quand nous cliquons 2x sur la liste,
        /// récupère la selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lstUsers_DoubleClick(object sender, EventArgs e)
        {
            if (_frmView.lstUsers.SelectedIndex != -1)
            {
                try
                {
                    string person = _frmView.lstUsers.SelectedItem.ToString().Replace(" ","").Replace("Connecté-","").Replace("Déconnecté-","");
                    person = person.Substring(0, person.IndexOf("("));
                    UpdateLstChat(person);
                    OpenChat(person);
                    _frmView.lstUsers.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show( "Veuillez reporter le message suivant:\n\n Click: " + ex.Message, "Erreur!");
                }

            }
        }
        /// <summary>
        /// quand la fenêtre est fermée, on arrête toutes 
        /// les tâches qui tournent en arrière plan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="form"></param>
        public void frmView_FormClosed(object sender, FormClosedEventArgs e, frmView form)
        {
            try
            {
                if(form.Name=="frmAccueil")
                {
                    this._model.NetworkStatus(false);
                    this._model.StopNetwork();
                    this._thReader.Interrupt();
                    this._thReader.Abort();
                    _model.LogExit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( "Veuillez reporter le message suivant:\n\n Fermeture1:" + ex.Message, "Erreur!");
            }
        }
        /// <summary>
        /// quand l'utilisateur clique sur envoyer le message,
        /// son nom est envoyé avec le message
        /// </summary>
        /// <param name="sender">objet qui enclanche l'évènement</param>
        /// <param name="e">évènement lié à l'objet</param>
        /// <param name="form">le formulaire qui envoie l'évènement</param>
        public void frmChat_cmdSender_Click(object sender, EventArgs e, frmView form)
        {
            try
            {
                if (!string.IsNullOrEmpty(form.rtbSender.Text))
                {
                    if (_model.lstOfUsers.Find(x => x.Name == form.Text).Status == "1")
                        _model.NetworkSender(form.Name, form.Text, form.rtbSender.Text);
                    else
                        _model.SendMessage(form.Text, form.rtbSender.Text);
                }
                form.rtbSender.Clear();
                form.rtbSender.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show( "Veuillez reporter le message suivant:\n\n Envoyeur: " + ex.Message, "Erreur!");
            }
        }

        /// <summary>
        /// timer qui raffraîchi toutes les secondes le contenu sur le chat
        /// </summary>
        /// <param name="sender">objet qui enclanche l'évènement</param>
        /// <param name="e">évènement lié à l'objet</param>
        /// <param name="form">le formulaire qui envoie l'évènement</param>
        public void frmChat_tmrMsg_Timer(object sender, EventArgs e, frmView form)
        {
            try
            {
            if (form.timerStatus)
                {
                    if (_model.MoreMessage(form.Text) > _lstOfUsers.Find(x => x.Name == form.Text).totMsg)
                    {
                        _lstOfUsers.Find(x => x.Name == form.Text).totMsg = _model.MoreMessage(form.Text);
                        
                        form.rtbReader.Clear();
                        foreach (Message message in _model.LstOfMessageHistory)
                            if(message.From == form.Text || message.To == form.Text)
                                form.rtbReader.AppendText("\r\n" + message.From + ": " + message.Msg);

                        foreach (Message message in _model.LstOfMessage)
                        {
                            if (message.From == "Leave:" + form.Text || message.To == "Leave:" + form.Text)//quand quelqu'un quitte l'application
                            {
                                _model.SaveDiscution(_model.LstOfMessage, form.Text);
                                foreach (Message msg in _model.GetHistory(form.Text))
                                    _model.LstOfMessageHistory.Add(msg);

                                recipientExit = true;

                                //enlever de la liste tous les messages destinés à la personne et de la personne
                                _model.LstOfMessage.RemoveAll(x => x.To == form.Text || x.From == form.Text || x.To != "Leave:" + form.Text || x.From != "Leave:" + form.Text);
                                FillLstUsers();
                                break;
                            }
                            else if (message.From == form.Text || message.To == form.Text)//discution avec quelqu'un
                            {
                                form.rtbReader.AppendText("\r\n" + message.From + ": " + message.Msg);
                            }
                        }
                        form.rtbReader.ScrollToCaret();
                    }
                }
                else
                {
                    _model.LstOfMessage.Clear();
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show( "Veuillez reporter le message suivant:\n\n MsgTimer: " + ex.Message, "Erreur!");
                form.Close();
            }
        }

        /// <summary>
        /// timer qui raffraîchi toutes les secondes le contenu sur le chat
        /// </summary>
        /// <param name="sender">objet qui enclanche l'évènement</param>
        /// <param name="e">évènement lié à l'objet</param>
        /// <param name="form">le formulaire qui envoie l'évènement</param>
        public void frmChat_clossing(object sender, EventArgs e, frmView form)
        {
            try
            {
                if (form.Name != "frmAccueil")
                {
                    form.tmrMsg.Stop();
                    form.timerStatus = false;
                    if (!recipientExit)
                    {
                        bool connected = true;
                        if (_model.lstOfUsers.Find(x => x.Name == form.Text).Status == "0")
                            connected=false;

                        _model.NewtworkLeaveApplication(form.Name, form.Text, connected);
                    }
                    recipientExit = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show( "Veuillez reporter le message suivant:\n\n Fermeture2: L'utilisateur "+ form.Text+" a déjà quitté l'application", "Erreur!");
            }
        }
        #endregion Public methods
    }
}
