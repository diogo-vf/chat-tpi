﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          User
    ///   Description:    Classe permettant de créer un utilisateur avec les données suivantes:
    ///                   Nom, adresse réseau (IP), statut (connecté ou non), nombre de messages manqués
    ///                   et le nombre de messages envoyés.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    public class User
    {
        private string _name;
        private string _ip;
        private string _status;
        private int _msgMissed;
        private int _totMsg;
        #region Accessors
        /// <summary>
        /// accesseur permettant de définir et connaître le nom de la personne
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        /// <summary>
        /// accesseur permettant de définir et connaître l'IP de la personne
        /// </summary>
        public string IP
        {
            get
            {
                return _ip;
            }
            set
            {
                _ip = value;
            }
        }
        /// <summary>
        /// accesseur permettant de définir et connaître si la personne est connectée ou non
        /// </summary>
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        /// <summary>
        /// accesseur permettant de définir et connaître le nombre de messages manqués de la par de cette personne
        /// </summary>
        public int MessagesMissed
        {
            get
            {
                return _msgMissed;
            }
            set
            {
                _msgMissed = value;
            }
        }
        /// <summary>
        /// accesseur permettant de définir et connaître le nombre de messages manqués de la par de cette personne
        /// </summary>
        public int totMsg
        {
            get
            {
                return _totMsg;
            }
            set
            {
                _totMsg = value;
            }
        }
        #endregion Accessors
        #region Public method
        /// <summary>
        /// méthode utilisée pour la comparaison des valeurs lors des test unitaires
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            User user = (User)obj;

            return (this._name==user.Name && this._ip==user.IP && this.MessagesMissed==user.MessagesMissed && this.Status==user.Status);
        }
        #endregion Public method
    }
}
