﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          DB
    ///   Description:    classe contenant toutes les méthodes communiquant avec la base de données.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    public class DB
    {
        private MySqlConnection _db;
        #region Constructor
        /// <summary>
        /// contructeur qui nous permet de créer un lien avec la base de données
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="dbName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public DB(string ip, string dbName, string username, string password)
        {
            // Création de la chaîne de connexion à la db
            string connectionString = "SERVER="+ip+"; DATABASE="+dbName+"; UID="+username+"; PASSWORD="+password+"";
            this._db = new MySqlConnection(connectionString);
        }
        #endregion Constructor
        #region Public methods
        /// <summary>
        /// insére ou modifie dans la base de données
        /// le nom, l'adresse ip et si l'utilisateur est connecté ou non
        /// </summary>
        /// <param name="myIP"></param>
        /// <param name="status"></param>
        public void NetworkStatus(string myIP, bool status)
        {
            //ouverture de la connection avec la db
            this._db.Open();

            //création du lien avec la db pour notre commande
            MySqlCommand cmd = this._db.CreateCommand();

            //requête SQL
            cmd.CommandText = "INSERT INTO `User` (`name`,`ip`,`status`) VALUES('" + Environment.UserName + "', '" + myIP + "', "+status.ToString()+") ON DUPLICATE KEY UPDATE `ip`= '" + myIP + "', `status`= "+status.ToString()+"";

            //on exécute la commande
            cmd.ExecuteNonQuery();

            //on ferme la connexion
            this._db.Close();
        }
        /// <summary>
        /// récupère dans la db toutes les personnes
        /// utilisant l'application
        /// </summary>
        /// <returns></returns>
        public List<User> LstOfUsers()
        {
            List<User> lstUsers = new List<User>();
            //ouverture de la connection avec la db
            this._db.Open();

            //création du lien avec la db pour notre commande
            MySqlCommand cmd = this._db.CreateCommand();
            MySqlDataReader rdr;
            //requête SQL
            cmd.CommandText = @"SELECT `idUser`,`name`,`ip`,`status`, (SELECT COUNT(`read`)  FROM `Message`  WHERE `fkUserFrom`= `idUser` AND `read`= FALSE )  AS MsgMissed FROM `User`";
            //on exécute la commande et on garde toutes les données
            rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                //on crée la liste des utilisateurs
                lstUsers.Add(new User { Name = rdr["name"].ToString(), IP = rdr["ip"].ToString(), Status = rdr["status"].ToString(), MessagesMissed = int.Parse(rdr["MsgMissed"].ToString()) });
            }
            rdr.Dispose();
            //on ferme la connexion
            this._db.Close();

            return lstUsers;
        }
        /// <summary>
        /// permet d'enregistrer les actions d'un utilisateur dans la base de données
        /// </summary>
        /// <param name="information"></param>
        public void Log(string information)
        {
            //ouverture de la connection avec la db
            this._db.Open();

            //création du lien avec la db pour notre commande
            MySqlCommand cmd = this._db.CreateCommand();

            //requête SQL
            cmd.CommandText = "INSERT INTO `Log` (`username`,`information`,`time`) VALUES('" + Environment.UserName + "', '" + information.Replace("'", "''") + "','"+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')";

            //on exécute la commande
            cmd.ExecuteNonQuery();

            //on ferme la connexion
            this._db.Close();
        }
        /// <summary>
        /// sauvegarde tous les messages dans la base de données
        /// </summary>
        /// <param name="lstOfMsg">liste d'objets de type message</param>
        /// <param name="username">nom de la personne qui a quitté l'application</param>
        /// <param name="read">permet de savoir si le message a été lu ou pas</param>
        public void SaveDiscution(List<Message> lstOfMsg, string username, bool read=true )
        {
            //ouverture de la connection avec la db
            this._db.Open();

            //création du lien avec la db pour notre commande
            MySqlCommand cmd = this._db.CreateCommand();

            //requête SQL
            foreach (Message msg in lstOfMsg)
            {
                if (msg.From == username || msg.To == username)
                {
                    cmd.CommandText = @"INSERT INTO `Message`(fkUserFrom,fkUserTo,`message`,`time`,`read`)
                                        VALUES((SELECT idUser FROM `User` WHERE `name`= '" + msg.From.Replace("Leave:","") + "'),(SELECT idUser FROM `User` WHERE `name`= '" + msg.To.Replace("Leave:", "") + "'), '" + msg.Msg.Replace("'", "''") + "', '" + msg.DateTime + "', "+read.ToString()+")";
                    cmd.ExecuteNonQuery();
                }
            }

            //on exécute la commande

            //on ferme la connexion
            this._db.Close();
        }
        /// <summary>
        /// récupération des messages entre le destinataire et nous
        /// </summary>
        /// <param name="username">nom du destinataire</param>
        /// <returns></returns>
        public List<Message> GetHistory(string username)
        {
            List<Message> lstMsg = new List<Message>();
            //ouverture de la connection avec la db
            this._db.Open();

            //création du lien avec la db pour notre commande
            MySqlCommand cmd = this._db.CreateCommand();
            MySqlDataReader rdr;
            //requête SQL
            cmd.CommandText = @"
                SELECT `message`, `time`, `From`.`name` AS `userFrom`, `To`.`name` AS `userTo`
                FROM `Message` 
                INNER JOIN `User` `From` ON `Message`.`fkUserFrom` = `From`.`idUser`
                INNER JOIN `User` `To` ON `Message`.`fkUserTo` = `To`.`idUser`
                WHERE `fkUserFrom`=(SELECT `idUser` FROM `User` WHERE `name`='"+Environment.UserName+"') AND `fkUserTo`=(SELECT `idUser` FROM `User` WHERE `name`='"+username+"') OR `fkUserFrom`=(SELECT `idUser` FROM `User` WHERE `name`='"+username+"') AND `fkUserTo`=(SELECT `idUser` FROM `User` WHERE `name`='"+Environment.UserName+"')";
            //on exécute la commande et on garde toutes les données
            rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                //on crée la liste des utilisateurs
                lstMsg.Add(new Message{ From=rdr["userFrom"].ToString(), To = rdr["userTo"].ToString(), DateTime= rdr["time"].ToString(), Msg= rdr["message"].ToString() });
            }
            rdr.Dispose();
            //on ferme la connexion
            this._db.Close();

            MsgReaded(username);

            return lstMsg;
        }   
        /// <summary>
        /// Envoie de messages directement sur la db
        /// </summary>
        /// <param name="username">destinataire</param>
        /// <param name="msg">message</param>
        public void SendMessage(string username, string msg)
        {
            //ouverture de la connection avec la db
            this._db.Open();

            //création du lien avec la db pour notre commande
            MySqlCommand cmd = this._db.CreateCommand();
           
            cmd.CommandText = @"INSERT INTO `Message`(fkUserFrom,fkUserTo,`message`,`time`,`read`)
                                VALUES((SELECT idUser FROM `User` WHERE `name`= '" + Environment.UserName + "'),(SELECT idUser FROM `User` WHERE `name`= '" + username + "'), '" + msg.Replace("'", "''") + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', false )";
            cmd.ExecuteNonQuery();
            //on exécute la commande

            //on ferme la connexion
            this._db.Close();
        }
        #endregion Public methods
        #region Private methods
        private void MsgReaded(string username)
        {
            this._db.Open();

            //création du lien avec la db pour notre commande
            MySqlCommand cmd = this._db.CreateCommand();

            cmd.CommandText = @"UPDATE `Message` SET `read`= TRUE
                                WHERE `fkUserFrom`= (SELECT `idUser` FROM `User` WHERE `name`= '" + username + "') AND `fkUserTo`= (SELECT `idUser` FROM `User` WHERE `name`= '" + Environment.UserName + "')";
            cmd.ExecuteNonQuery();
            //on exécute la commande

            //on ferme la connexion
            this._db.Close();



        }
        #endregion Private methods
    }
}
