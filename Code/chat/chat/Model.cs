﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          Model
    ///   Description:    Contient toutes les données de l'application, elle permet de se connecter à la base de
    ///                   données et de faire le lien avec la partie réseau.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    public class Model
    {
        private DB _db;
        private NetworkCommunication _ntw;
        private string _messageFrom;
        private bool _ntwRunning;
        private List<Message> _lstOfMessage;
        private List<Message> _lstOfMessageHistory;

        #region Constructor
        /// <summary>
        /// initialise la base de données et la classe 
        /// qui me permet de manipuler les sockets
        /// </summary>
        public Model()
        {
            _db = new DB("172.17.101.71", "Chat", "ChatAdmin", "Pa$$w0rd");
            _ntw = new NetworkCommunication();
            _lstOfMessage = new List<Message>();
            _lstOfMessageHistory = new List<Message>();
            NetworkStatus(true);
        }
        #endregion Constructor
        #region Public methods
        /// <summary>
        /// Permet de savoir qu'on est connecté ou deconnecté et quel est notre adresse IP
        /// </summary>
        /// <param name="status"></param>
        public void NetworkStatus(bool status)
        {
            string ip = "";
            if (status)
                ip = _ntw.MyIP;
            else
                ip = "";

            _db.NetworkStatus(ip,status);
            _db.Log("Connection à l'application éffectuée");
        }

        /// <summary>
        /// on démarre le serveur qui nous permettra d'écouter les sockets venant sur le réseau
        /// </summary>
        public void StartNetwork()
        {
            //on prépare la lecture en boucle des sockets
            _ntwRunning = true;
            //on demarre la lecture des sockets
            _ntw.StartServer();
            _db.Log("Démarrage de l'écoute sur le réseau");
        }
        /// <summary>
        /// on arrête l'écoute des sockets sur le réseau
        /// </summary>
        public void StopNetwork()
        {
            //on arrête la boucle de lecture des sockets
            _ntwRunning = false;
            //on peut maintenant fermer le serveur
            _ntw.StopServer();
            _db.Log("Arrêt de l'écoute sur le réseau");
        }
        /// <summary>
        /// on écoute en continu les informations reçues depuis le réseau
        /// </summary>
        public void NetworkReader()
        {
            //on écoute en continu le réseau
            _ntw.SocketReader();            
        }
        /// <summary>
        /// Méthode enregistrant le message dans la liste _lstMsg
        /// et envoie le message au destinataire 
        /// </summary>
        /// <param name="recipientIP">adresse IP du destinataire</param>
        /// <param name="message"></param>
        /// <param name="to">destinataire du message</param>
        public void NetworkSender(string recipientIP, string to, string message)
        {
            message = message.TrimEnd('\r', '\n', ' ');
            
            _lstOfMessage.Add(new Message { From = Environment.UserName, To = to, Msg = message, DateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") });
        
            _ntw.SocketSender(recipientIP, Environment.UserName + ";" + message + ";" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }
        /// <summary>
        /// met dans la liste le dernier message
        /// </summary>
        public void NetworkReceiver()
        {
            if (!string.IsNullOrEmpty(_ntw.Message))
            {
                string message = _ntw.Message.Replace("\0","");
                Console.WriteLine(message);
                _messageFrom = message.Substring(0, message.IndexOf(";"));
                _lstOfMessage.Add(new Message { From = message.Substring(0, message.IndexOf(";")), To = Environment.UserName, Msg = message.Substring(message.IndexOf(";") + 1, (message.LastIndexOf(";") - message.IndexOf(";") - 1)), DateTime = message.Substring(message.LastIndexOf(";") + 1) });
                _ntw.Message = null;
                message = null;
            }
        }
        /// <summary>
        /// envoie une message spécifique à la fermeture de l'application
        /// </summary>
        /// <param name="recipientIP">ip avec qui nous discutions</param>
        /// <param name="userConnected">permet d'envoyer le message de déconnection si nous sommes déconnecté</param>
        /// <param name="username">avec qui nous discutions</param>
        public void NewtworkLeaveApplication(string recipientIP, string username, bool userConnected = true)
        {
            if (userConnected)
                _ntw.SocketSender(recipientIP, "Leave:" + Environment.UserName + "; vient de quitter l'application...;" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            _db.Log("Fin de conversation avec " + username);
        }

        /// <summary>
        /// permet de mettre dans le log avec qui l'utilisateur discute
        /// </summary>
        /// <param name="user"></param>
        public void DiscutWith(string user)
        {
            _db.Log("Conversation avec " + user);
        }
        /// <summary>
        /// permet de mettre dans le log la fermeture d'une conversation entre 2 personnes
        /// </summary>
        /// <param name="user"></param>
        public void EndDiscutWith(string user)
        {
            _db.Log("Fin de la conversation avec " + user);
        }
        /// <summary>
        /// insert all discution with specific person
        /// </summary>
        /// <param name="lstOfMsg">liste of messages</param>
        /// <param name="username">personne envoyant les message</param>
        /// <param name="read">message lu ou non?</param>
        public void SaveDiscution(List<Message> lstOfMsg, string username, bool read = true)
        {
            _db.SaveDiscution(lstOfMsg, username, read);

        }
        /// <summary>
        /// récupère les messages entre le destinataire et nous dans la DB
        /// </summary>
        /// <param name="username">nom du destinataire</param>
        public List<Message> GetHistory(string username)
        {
            return _db.GetHistory(username);
        }
        /// <summary>
        /// calcule le nombre total de messages par utilisateur
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public int MoreMessage(string username)
        {
            int i = 0;
            foreach (Message msg in _lstOfMessage)
            {
                if (msg.From == username || msg.To == username || msg.From == "Leave:" + username || msg.To == "Leave:" + username)
                    i++;
            }
            return i;
        }
        /// <summary>
        /// envoie de messages quand l'utilisateur n'est pas connecté
        /// </summary>
        /// <param name="username">destinataire</param>
        /// <param name="msg">message</param>
        public void SendMessage(string username, string msg)
        {
            msg = msg.TrimEnd('\r', '\n', ' ');
            _lstOfMessage.Add(new Message { From = Environment.UserName, To = username, Msg = msg, DateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") });
            _db.SendMessage(username, msg);
        }

        /// <summary>
        /// Log de fin d'utilisation de la messaderie
        /// </summary>
        public void LogExit()
        {
            _db.Log("Vient de fermer la messagerie");
        }
        #endregion Public methods
        #region Accessors
        /// <summary>
        /// retourne une liste d'utilisateurs
        /// </summary>
        /// <returns></returns>
        public List<User> lstOfUsers
        {
            get
            {
                return _db.LstOfUsers();
            }
        }
        /// <summary>
        /// accesseur permetant de connaître notre adresse IP
        /// </summary>
        public string IP
        {
            get
            {
                return _ntw.MyIP;
            }
        }
        /// <summary>
        /// accesseur permettant de savoir si le serveur est encours d'écoute
        /// </summary>
        public bool NtwRunning
        {
            get
            {
                return _ntwRunning;
            }
            set
            {
                _ntwRunning = value;
            }
        }
        /// <summary>
        /// accesseur retournant qui a envoyé le dernier message réseau
        /// </summary>
        public string MessageFrom
        {
            get
            {
                return _messageFrom;
            }
        }
        /// <summary>
        /// accesseur permettant de voir les messages et
        /// de mettre à jour la liste de messages
        /// </summary>
        public List<Message> LstOfMessage
        {
            get
            {
                return _lstOfMessage;
            }
            set
            {
                _lstOfMessage = value;
            }
        }
        /// <summary>
        /// accesseur permettant de voir les anciens messages
        /// </summary>
        public List<Message> LstOfMessageHistory
        {
            get
            {
                return _lstOfMessageHistory;
            }
        }
        #endregion Accessors
    }
}
