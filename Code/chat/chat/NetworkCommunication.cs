﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          NetworkCommunication
    ///   Description:    Classe utilisant les sockets TCP ( packets réseau ) sur le port 8008.
    ///                   Elle permet d'initier une écoute sur le réseau et d'envoyer des messages
    ///                   sur le réseau.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    public class NetworkCommunication
    {
        private static IPAddress _myIp;
        private static TcpListener _server;
        private static TcpClient _client;
        private string _message;
        #region Constructor
        /// <summary>
        /// on récupère l'ip du client et on prépare 
        /// la création du serveur d'écoute et d'envoie
        /// </summary>
        public NetworkCommunication()
        {
            //on récupère notre adresse IPv4
            _myIp = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
            _server = new TcpListener(_myIp, 8008);
            _client = default(TcpClient);
        }
        #endregion Constructor
        #region Public method
        /// <summary>
        /// on crée un byte contenant notre message
        /// et on l'envoie à la bonne IP
        /// </summary>
        /// <param name="serverIP"></param>
        /// <param name="message"></param>
        public void SocketSender(string serverIP, string message)
        {
            int port = 8008;
            TcpClient client = new TcpClient(serverIP, port);
            //on compte le nombre de charactères dans notre message
            int byteCount = Encoding.UTF8.GetByteCount(message);
            byte[] sendData = new byte[byteCount];

            //on converti le tout en utf8
            sendData = Encoding.UTF8.GetBytes(message);
            NetworkStream stream = client.GetStream();
            //on envoie le message
            stream.Write(sendData, 0, sendData.Length);

            stream.Close();
            client.Close();
        }
        /// <summary>
        /// méthode écoutant tout le trafique entrant sur le port définit plus tôt
        /// </summary>
        public void SocketReader()
        {
            try
            {                
                //on accept les requetes venant du serveur
                _client = _server.AcceptTcpClient();

                //byte contenant le buffer
                byte[] receivedBuffer = new byte[9999];
                NetworkStream stream = _client.GetStream();

                //on lis l'entièreté du message
                stream.Read(receivedBuffer, 0, receivedBuffer.Length);
                _message = Encoding.UTF8.GetString(receivedBuffer);    
                Console.WriteLine(_message);
            }
            catch(Exception)
            {

            }
        }
        /// <summary>
        /// on démarre le serveur d'écoute
        /// </summary>
        public void StartServer()
        {
            _server.Start();
        }
        /// <summary>
        /// on arrête le serveur
        /// </summary>
        public void StopServer()
        {
            _server.Stop();
        }
        #endregion Public method
        #region Accessor
        /// <summary>
        /// accesseur fournissant notre adresse ip 
        /// </summary>
        public string MyIP
        {
            get
            {
                return _myIp.ToString();
            }
        }
        /// <summary>
        /// accesseur permettant de lire le contenu du message
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }
        #endregion Accessor
    }
}
