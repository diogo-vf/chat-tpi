﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          Message
    ///   Description:    Permet de créer un message. Elle contient:
    ///                   L'envoyeur, le destinataire, la date et l'heure du message et le message.
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    public class Message
    {
        private string _message;
        private string _from;
        private string _to;
        private string _dateTime;
        #region Accessor
        /// <summary>
        /// Accesseur permettant de lire ou écrire le message
        /// </summary>
        public string Msg
        {
            get
            {
                return _message;
            }
            set
            {
               _message = value;
            }
        }
        /// <summary>
        /// Accesseur permettant de lire ou écrire à qui appartient le message
        /// </summary>
        public string From
        {
            get
            {
                return _from;
            }
            set
            {
                _from = value;
            }
        }
        /// <summary>
        /// Accesseur permettant de lire ou écrire à qui le message est envoyé
        /// </summary>
        public string To
        {
            get
            {
                return _to;
            }
            set
            {
                _to = value;
            }
        }
        /// <summary>
        /// Accesseur permettant de lire ou écrire à quand le message a été envoyé
        /// </summary>
        public string DateTime
        {
            get
            {
                return _dateTime;
            }
            set
            {
                _dateTime = value;
            }
        }
        #endregion Accessor
    }
}
