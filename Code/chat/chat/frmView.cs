﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace chat
{
    /// <summary>
    ///-------------------------------------------------------------------------------------------------------------------
    ///
    ///   Namespace:      Chat
    ///   Class:          frmView
    ///   Description:    Formulaire permettant d'afficher le chat, comme les messages.                   
    ///   Author:         Diogo Vieira Ferreira
    ///   Date:           28.05.19
    ///   
    ///-------------------------------------------------------------------------------------------------------------------
    /// </summary>
    public partial class frmView : Form
    {
        private Controler _ctrl;
        /// <summary>
        /// on renomme notre formulaire et on l'adapte.
        /// </summary>
        /// <param name="ctrl">on récupère le controleur pour que la gestion des évènements se fasse par le biai de celui-ci</param>
        /// <param name="text">titre visible sur la fenêtre</param>
        /// <param name="name">nom d'appellation pour le code</param>
        public frmView(Controler ctrl, string text = "View", string name = "frmView")
        {
            this._ctrl = ctrl;
            this.InitializeComponent();
            this.Name = name;
            this.Text = text;
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

    }
}
