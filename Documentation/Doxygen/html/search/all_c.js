var searchData=
[
  ['savediscution',['SaveDiscution',['../classchat_1_1_d_b.html#ac3d12d4ca168b39831afd24720af951a',1,'chat.DB.SaveDiscution()'],['../classchat_1_1_model.html#a19e6cd3138348f235a9acb0a3adef16d',1,'chat.Model.SaveDiscution()']]],
  ['secondinterface',['SecondInterface',['../classchat_1_1frm_view.html#a5159ba65d2bfc099b267ccb11c196293',1,'chat::frmView']]],
  ['sendmessage',['SendMessage',['../classchat_1_1_d_b.html#a78a009b9a3e987e097aa366f89261950',1,'chat.DB.SendMessage()'],['../classchat_1_1_model.html#a3e890e0795998078b577fd7efa963643',1,'chat.Model.SendMessage()']]],
  ['socketreader',['SocketReader',['../classchat_1_1_network_communication.html#a8a4bf9c66f72eacee59414161799af07',1,'chat::NetworkCommunication']]],
  ['socketsender',['SocketSender',['../classchat_1_1_network_communication.html#a6f62f72d131f17e6ede35435680fbedd',1,'chat::NetworkCommunication']]],
  ['startnetwork',['StartNetwork',['../classchat_1_1_model.html#a96b0f47eebf486eaf4e59d9199bb36f1',1,'chat::Model']]],
  ['startserver',['StartServer',['../classchat_1_1_network_communication.html#ab3bc2f828d991a657a6febc70bd91f73',1,'chat::NetworkCommunication']]],
  ['status',['Status',['../classchat_1_1_user.html#abe85b9e9b5315370646725a0cbf4868c',1,'chat::User']]],
  ['stopnetwork',['StopNetwork',['../classchat_1_1_model.html#a50509098f947367577f0829770160bd7',1,'chat::Model']]],
  ['stopserver',['StopServer',['../classchat_1_1_network_communication.html#a6bdd6c1d0f67cea3e0f8ae0337dc0191',1,'chat::NetworkCommunication']]]
];
