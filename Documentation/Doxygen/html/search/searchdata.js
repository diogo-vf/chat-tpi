var indexSectionsWithContent =
{
  0: "acdefgilmnorstu",
  1: "acdfmntu",
  2: "c",
  3: "cdefgilmnostu",
  4: "clnrt",
  5: "dfilmnst",
  6: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Espaces de nommage",
  3: "Fonctions",
  4: "Variables",
  5: "Propriétés",
  6: "Pages"
};

